package com.apress.helloworld;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/test")
public class TestController {

	
	@RequestMapping(value="/form.html",method = RequestMethod.GET)
	public ModelAndView getF(){
		
		ModelAndView model = new ModelAndView("test");
		return model;
	}
	
	@RequestMapping(value="/submitform.html",method = RequestMethod.POST)
	public ModelAndView submit(@RequestParam("login") String myLogin){
		
		ModelAndView model2 = new ModelAndView("showFormResult");
		
		model2.addObject("ownLogin", myLogin);
		
		return model2;
	}
	
	
	
}
